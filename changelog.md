# Changelog
All notable changes to this project will be documented in this file.

## [1.2.0] - 2024-11-21
* updated dependencies
* Bump Java version (to 11)
* adjust Gitlab CI

## [1.1.0] - 2021-11-18
* Issue #1 - Files are created on every call, not reused

## [1.0.0] - 2020-07-21
* Initial release

[master]: https://gitlab.com/axual-public/vault-keystore-provider/-/compare/1.2.0...master
[1.2.0]: https://gitlab.com/axual-public/vault-keystore-provider/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/axual-public/vault-keystore-provider/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/axual-public/vault-keystore-provider/-/tree/1.0.0