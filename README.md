# Vault Keystore Provider
[![Pipeline Status](https://gitlab.com/axual-public/vault-keystore-provider/badges/master/pipeline.svg)](https://gitlab.com/axual-public/vault-keystore-provider/commits/master) 
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-keystore-provider&metric=alert_status&token=d82023fb6fc7ff6f0269f89e1f1cd37f18fd4167)](https://sonarcloud.io/dashboard?id=axual-public-vault-keystore-provider)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-keystore-provider&metric=coverage&token=d82023fb6fc7ff6f0269f89e1f1cd37f18fd4167)](https://sonarcloud.io/dashboard?id=axual-public-vault-keystore-provider)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-keystore-provider&metric=sqale_rating&token=d82023fb6fc7ff6f0269f89e1f1cd37f18fd4167)](https://sonarcloud.io/dashboard?id=axual-public-vault-keystore-provider)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

A configuration provider for Kafka Connect to retrieve Keystore data from HashiCorp Vault and create keystore and truststore files.

[[_TOC_]]

How to use
----------
The ConfigProvider is a means for Kafka Applications to retrieve secret configuration options and provide them to the implementations without showing the contents in the output. 
The Keystore Provider for HashiCorp Vault can connect to a HashiCorp Vault installation and create keystore files from the PEM data stored there.
The truststore file is provided in the configuration and will be used for the Kafka truststore settings.


Configuration Options
---------------------
The configuration definition can be found in the class ```io.axual.utilities.config.providers.VaultKeyStoreProviderConfig```

| Config                          |  Type     | Required |Default                   | Description   |
|:--------------------------------|:---------:|:--------:|:------------------------:|:--------------|
| ***address***                   | STRING    | Yes      |                          | The URL where HashiCorp Vault can be reached  |
| ***timeout.open***              | INTEGER   | No       | ```30```                 | The number of seconds to wait before giving up on establishing an HTTP(S) connection to the Vault server  |
| ***timeout.read***              | INTEGER   | No       | ```30```                 | After an HTTP(S) connection has already been established, this is the number of seconds to wait for all data to finish downloading  |
| ***namespace***                 | STRING    | No       | ```null```               | Sets a global namespace to the Vault server instance |
| ***global.engine.version***     | INTEGER   | No       | ```2```                  | Sets the KV Secrets Engine version of the Vault server instance |
| ***prefix.path.depth***         | INTEGER   | No       | ```1```                  | Set the "path depth" of the prefix path |
| ***prefix.path***               | STRING    | No       | ```null```               | Set the "path depth" of the prefix path by specifying the path. /a/b/c would result in a prefix path depth of 3 |
| ***ssl.verify***                | BOOLEAN   | No       | ```true```               | Whether or not HTTPS connections to the Vault server should verify that a valid SSL certificate is being used |
| ***ssl.truststore.location***   | STRING    | No       | ```null```               | The path to a JKS keystore file, containing the Vault TLS Certificate or Certificate Authorities |
| ***ssl.truststore.password***   | PASSWORD  | No       | ```null```               | The password to access the JKS keystore file containing the Vault TLS Certificate or Certificate Authorities |
| ***auth.method***               | STRING    | No       | ```"APPROLE"```          | The authentication method to use. Valid values are APPROLE |
| ***approle.path***              | STRING    | No       | ```"approle"```          | The path on which the authentication is performed, following the "/v1/auth/" prefix (e.g. "approle") |
| ***approle.role.id***           | STRING    | No       | ```null```               | The vault role id to use for communicating with Vault |
| ***approle.secret.id***         | PASSWORD  | No       | ```null```               | The vault secret id to use for communicating with Vault |
| ***test.path***                 | STRING    | No       | ```null```               | The path of the data to retrieve from Vault during configuration |
| ***private.key.keyname***       | STRING    | No       | ```private.key```        | The key name in the vault where the private key data is stored |
| ***certificate.chain.keyname*** | STRING    | No       | ```certificate.chain```  | The key name in the vault where the chained certificate data stored |
| ***temporary.storage.dir***     | STRING    | No       | ```JVM Temporary Directory```  | The key name in the vault where the private key is stored |
| ***truststore.location***       | STRING    | No       | ```null```  | The truststore file to use for the truststore variables |
| ***truststore.password***       | STRING    | No       | ```null```  | The password of truststore file to use for the truststore variables |

Known Limitations
-----------------
* Does not implement the Kafka ConfigProvider subscribe methods
* AppRole authentication only
* No PKI support
* Supports only unencrypted keys

License
-------
Configuration Provider for HashiCorp Vault is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).