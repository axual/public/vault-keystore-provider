path "resources/*" {
  capabilities = [
    "read",
    "create",
    "update",
    "delete"]
}
