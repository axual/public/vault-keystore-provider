package io.axual.utilities.config.providers.keystore;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_CA;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_CA_FINGERPRINT;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_CERTIFICATES;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_ENCRYPTED;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_UNENCRYPTED;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_UNENCRYPTED_FINGERPRINT_1;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_MULTIPLE_CA;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertCACertificate;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyCertificates;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyUnencrypted;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertMultipleCACertificate;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.fingerprint;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.axual.utilities.config.providers.exceptions.KeyStoreCreatorException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;

class KeyStoreCreatorTest {

  static final String CERTIFICATE_DATA_CA_NAME;
  static final CertificateData CERTIFICATE_DATA_CA;

  static final String CERTIFICATE_DATA_MULTIPLE_CA_NAME;
  static final CertificateData CERTIFICATE_DATA_MULTIPLE_CA;

  static final String CERTIFICATE_DATA_NULL_NAME;
  static final CertificateData CERTIFICATE_DATA_NULL;

  static final String KEY_DATA_NULL_KEY_NAME;
  static final KeyData KEY_DATA_NULL_KEY;
  static final Password KEY_DATA_NULL_KEY_PASSWORD;

  static final String KEY_DATA_NULL_CERTIFICATE_NAME;
  static final KeyData KEY_DATA_NULL_CERTIFICATE;
  static final Password KEY_DATA_NULL_CERTIFICATE_PASSWORD;

  static final String KEY_DATA_ENCRYPTED_NAME;
  static final KeyData KEY_DATA_ENCRYPTED;
  static final Password KEY_DATA_ENCRYPTED_PASSWORD;

  static final String KEY_DATA_UNENCRYPTED_NAME;
  static final KeyData KEY_DATA_UNENCRYPTED;
  static final Password KEY_DATA_UNENCRYPTED_PASSWORD;

  static {

    CERTIFICATE_DATA_CA_NAME = "test_certificate_ca";
    CERTIFICATE_DATA_CA = new CertificateData(PEM_CA);

    CERTIFICATE_DATA_MULTIPLE_CA_NAME = "test_certificate_multiple_ca";
    CERTIFICATE_DATA_MULTIPLE_CA = new CertificateData(PEM_MULTIPLE_CA);

    CERTIFICATE_DATA_NULL_NAME = "test_certificate_null";
    CERTIFICATE_DATA_NULL = new CertificateData(null);

    KEY_DATA_UNENCRYPTED_NAME = "test_key_unencrypted";
    KEY_DATA_UNENCRYPTED_PASSWORD = new Password("Unencrypted");
    KEY_DATA_UNENCRYPTED = new KeyData(PEM_KEY_UNENCRYPTED, PEM_KEY_CERTIFICATES,
        KEY_DATA_UNENCRYPTED_PASSWORD);

    KEY_DATA_ENCRYPTED_NAME = "test_key_encrypted";
    KEY_DATA_ENCRYPTED_PASSWORD = new Password("Encrypted");
    KEY_DATA_ENCRYPTED = new KeyData(PEM_KEY_ENCRYPTED, PEM_KEY_CERTIFICATES,
        KEY_DATA_ENCRYPTED_PASSWORD);

    KEY_DATA_NULL_KEY_NAME = "test_key_null_key";
    KEY_DATA_NULL_KEY_PASSWORD = new Password("NullKey");
    KEY_DATA_NULL_KEY = new KeyData(null, PEM_KEY_CERTIFICATES,
        KEY_DATA_ENCRYPTED_PASSWORD);

    KEY_DATA_NULL_CERTIFICATE_NAME = "test_key_null_certificate";
    KEY_DATA_NULL_CERTIFICATE_PASSWORD = new Password("NullCertificate");
    KEY_DATA_NULL_CERTIFICATE = new KeyData(PEM_KEY_UNENCRYPTED, null,
        KEY_DATA_ENCRYPTED_PASSWORD);
  }

  @Test
  void createKeystoreAsTruststoreSingleCertificate()
      throws KeyStoreException, CertificateEncodingException {
    final Password truststorePassword = new Password("truststore");
    KeyStoreData trustStoreData = new KeyStoreData(null, truststorePassword);
    trustStoreData.putEntry(CERTIFICATE_DATA_CA_NAME, CERTIFICATE_DATA_CA);

    KeyStore truststore = KeyStoreCreator.INSTANCE.createKeystore(trustStoreData);

    Certificate ca = truststore.getCertificate(CERTIFICATE_DATA_CA_NAME);
    assertNotNull(ca);
    assertEquals(PEM_CA_FINGERPRINT, fingerprint(ca.getEncoded()));
  }

  @Test
  void createKeystoreAsTruststoreMultipleCertificates()
      throws KeyStoreException, CertificateEncodingException {
    final Password truststorePassword = new Password("truststore");
    KeyStoreData trustStoreData = new KeyStoreData(null, truststorePassword);
    trustStoreData.putEntry(CERTIFICATE_DATA_MULTIPLE_CA_NAME, CERTIFICATE_DATA_MULTIPLE_CA);

    KeyStore truststore = KeyStoreCreator.INSTANCE.createKeystore(trustStoreData);

    Certificate ca_0 = truststore.getCertificate(
        String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
            CERTIFICATE_DATA_MULTIPLE_CA_NAME, 0));
    assertNotNull(ca_0);
    assertEquals(PEM_CA_FINGERPRINT, fingerprint(ca_0.getEncoded()));

    Certificate ca_1 = truststore.getCertificate(
        String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
            CERTIFICATE_DATA_MULTIPLE_CA_NAME, 1));
    assertNotNull(ca_1);
    assertEquals(PEM_CA_FINGERPRINT, fingerprint(ca_1.getEncoded()));
  }


  @Test
  void createAndSaveKeystoreAsKeystore()
      throws KeyStoreException, CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException {
    final Password keystorePassword = new Password("keystore");
    final File target = File.createTempFile("keystore_", "_generated.jks");

    KeyStoreData keystoreData = new KeyStoreData(Paths.get(target.toURI()), keystorePassword);
    keystoreData.putEntry(KEY_DATA_UNENCRYPTED_NAME, KEY_DATA_UNENCRYPTED);
    keystoreData.putEntry(CERTIFICATE_DATA_MULTIPLE_CA_NAME, CERTIFICATE_DATA_MULTIPLE_CA);

    KeyStore keystore = KeyStoreCreator.INSTANCE.createKeystore(keystoreData, true);

    // Verify separate certificates
    Certificate ca_0 = keystore.getCertificate(
        String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
            CERTIFICATE_DATA_MULTIPLE_CA_NAME, 0));

    Certificate ca_1 = keystore.getCertificate(
        String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
            CERTIFICATE_DATA_MULTIPLE_CA_NAME, 1));
    assertMultipleCACertificate(ca_0, ca_1);

    // Verify key and key certificate chains
    Certificate[] certs = keystore.getCertificateChain(KEY_DATA_UNENCRYPTED_NAME);
    Key key = keystore
        .getKey(KEY_DATA_UNENCRYPTED_NAME, KEY_DATA_UNENCRYPTED_PASSWORD.value().toCharArray());

    assertKeyUnencrypted(key);
    assertKeyCertificates(certs);

    // Check keystore file
    assertTrue(target.exists());

    try (FileInputStream inputStream = new FileInputStream(target.getAbsolutePath())) {
      KeyStore ksFromFile = KeyStore.getInstance("JKS");
      ksFromFile.load(inputStream, keystorePassword.value().toCharArray());

      // Verify key and key certificate chains from file
      Certificate[] certsFromFile = ksFromFile.getCertificateChain(KEY_DATA_UNENCRYPTED_NAME);
      Key keyFromFile = keystore
          .getKey(KEY_DATA_UNENCRYPTED_NAME, KEY_DATA_UNENCRYPTED_PASSWORD.value().toCharArray());

      assertKeyUnencrypted(keyFromFile);
      assertKeyCertificates(certsFromFile);

      // Verify separate certificates
      Certificate caFromFile_0 = ksFromFile.getCertificate(
          String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
              CERTIFICATE_DATA_MULTIPLE_CA_NAME, 0));

      Certificate caFromFile_1 = ksFromFile.getCertificate(
          String.format(KeyStoreCreator.CERTIFICATE_CHAIN_NAME_PATTERN,
              CERTIFICATE_DATA_MULTIPLE_CA_NAME, 1));

      assertMultipleCACertificate(caFromFile_0, caFromFile_1);

    }

  }


  @Test
  void createKeystoreAsKeystore()
      throws KeyStoreException, CertificateEncodingException, UnrecoverableKeyException, NoSuchAlgorithmException {
    final Password keystorePassword = new Password("keystore");
    KeyStoreData keystoreData = new KeyStoreData(null, keystorePassword);
    keystoreData.putEntry(KEY_DATA_UNENCRYPTED_NAME, KEY_DATA_UNENCRYPTED);

    KeyStore keystore = KeyStoreCreator.INSTANCE.createKeystore(keystoreData);

    Certificate[] certs = keystore.getCertificateChain(KEY_DATA_UNENCRYPTED_NAME);
    assertKeyCertificates(certs);

    Key key = keystore
        .getKey(KEY_DATA_UNENCRYPTED_NAME, KEY_DATA_UNENCRYPTED_PASSWORD.value().toCharArray());
    assertKeyUnencrypted(key);
  }


  @Test
  void createKeyUnencrypted() {
    Key key = KeyStoreCreator.INSTANCE.createKey(KEY_DATA_UNENCRYPTED);
    assertNotNull(key);
    assertEquals(PEM_KEY_UNENCRYPTED_FINGERPRINT_1, fingerprint(key.getEncoded()));
  }

  @Test
  void createKeyEncrypted() {
    assertThrows(KeyStoreCreatorException.class,
        () -> KeyStoreCreator.INSTANCE.createKey(KEY_DATA_ENCRYPTED));
  }

  @Test
  void createKeyNullKey() {
    assertThrows(KeyStoreCreatorException.class,
        () -> KeyStoreCreator.INSTANCE.createKey(KEY_DATA_NULL_KEY));
  }

  @Test
  void getCertificateFromCertificateData() throws CertificateEncodingException {
    Certificate[] certificates = KeyStoreCreator.INSTANCE.createCertificates(CERTIFICATE_DATA_CA);
    assertCACertificate(certificates);
  }

  @Test
  void getCertificateFromCertificateDataNull() throws CertificateEncodingException {
    assertThrows(KeyStoreCreatorException.class,
        () -> KeyStoreCreator.INSTANCE.createCertificates(CERTIFICATE_DATA_NULL));
  }

  @Test
  void getCertificateFromKeyData() throws CertificateEncodingException {
    Certificate[] certificates = KeyStoreCreator.INSTANCE.createCertificates(KEY_DATA_UNENCRYPTED);
    assertKeyCertificates(certificates);
  }

  @Test
  void getCertificateFromKeyDataEncrypted() throws CertificateEncodingException {
    Certificate[] certificates = KeyStoreCreator.INSTANCE.createCertificates(KEY_DATA_ENCRYPTED);
    assertKeyCertificates(certificates);
  }

  @Test
  void getCertificateFromKeyDataNullKey() throws CertificateEncodingException {
    Certificate[] certificates = KeyStoreCreator.INSTANCE.createCertificates(KEY_DATA_NULL_KEY);
    assertKeyCertificates(certificates);
  }

  @Test
  void getCertificateFromKeyDataNullCertificate() throws CertificateEncodingException {
    assertThrows(KeyStoreCreatorException.class,
        () -> KeyStoreCreator.INSTANCE.createCertificates(KEY_DATA_NULL_CERTIFICATE));
  }
}
