package io.axual.utilities.config.providers.keystore;

/*-
 * ========================LICENSE_START=================================
 * vault-keystore-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;

class KeyDataTest {

  public static final String EXPECTED_KEY = "key";
  public static final String EXPECTED_CERT = "cert";
  public static final Password EXPECTED_PASSWORD = new Password("pass");

  @Test
  void getSetData() {
    KeyData certData = new KeyData(EXPECTED_KEY, EXPECTED_CERT, EXPECTED_PASSWORD);
    assertTrue(certData.getKeyPem().isPresent());
    assertEquals(EXPECTED_KEY, certData.getKeyPem().get());

    assertTrue(certData.getCertPem().isPresent());
    assertEquals(EXPECTED_CERT, certData.getCertPem().get());

    assertTrue(certData.getKeyPassword().isPresent());
    assertEquals(EXPECTED_PASSWORD, certData.getKeyPassword().get());
  }

  @Test
  void getNullData() {
    KeyData certData = new KeyData(null, null, null);
    assertFalse(certData.getKeyPem().isPresent());
    assertFalse(certData.getCertPem().isPresent());
    assertFalse(certData.getKeyPassword().isPresent());
  }
}
