package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import com.bettercloud.vault.response.LogicalResponse;

import org.apache.kafka.common.config.ConfigData;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.axual.utilities.config.providers.exceptions.VaultKeyStoreProviderException;
import io.axual.utilities.config.providers.keystore.KeyData;
import io.axual.utilities.config.providers.keystore.KeyStoreCreator;
import io.axual.utilities.config.providers.keystore.KeyStoreData;

import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_CERTIFICATES;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_UNENCRYPTED;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyCertificates;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyUnencrypted;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VaultKeyStoreProviderTest {

    public static final String EXPECTED_PRIVATE_KEY_KEYNAME = "privKey";
    public static final String EXPECTED_CERTIFICATE_CHAIN_KEYNAME = "certChain";
    public static final String EXPECTED_ADDRESS = "http://SomeAddress";
    public static final String EXPECTED_APP_ID = "appID";
    public static final String EXPECTED_SECRET_ID = "secretId";
    public static final String EXPECTED_TRUSTSTORE_LOCATION = "/tmp/somewhere/trust.jks";
    public static final String EXPECTED_TRUSTSTORE_PASSWORD = "truststorePassword";

    public static final String EXPECTED_PREFIX_1 = "pre.one";
    public static final String EXPECTED_PREFIX_2 = "pre.two.";
    public static final String EXPECTED_PREFIX_3 = "   pre.three.    ";
    public static final List<String> EXPECTED_PREFIXES = Arrays
            .asList(EXPECTED_PREFIX_1, EXPECTED_PREFIX_2, EXPECTED_PREFIX_3);

    private static final Map<String, Object> CONFIG_MINUMUM;

    static {
        Map<String, Object> props = new HashMap<>();
        props.put(VaultKeyStoreProviderConfig.VAULT_ADDRESS_CONFIG, EXPECTED_ADDRESS);
        props.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, EXPECTED_APP_ID);
        props
                .put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG,
                        EXPECTED_SECRET_ID);
        props.put(VaultKeyStoreProviderConfig.KEYSTORE_PRIVATE_KEY_KEYNAME_CONFIG,
                EXPECTED_PRIVATE_KEY_KEYNAME);
        props.put(VaultKeyStoreProviderConfig.KEYSTORE_CERTIFICATE_CHAIN_KEYNAME_CONFIG,
                EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        props.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_LOCATION_CONFIG,
                EXPECTED_TRUSTSTORE_LOCATION);
        props.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_PASSWORD_CONFIG,
                EXPECTED_TRUSTSTORE_PASSWORD);

        CONFIG_MINUMUM = Collections.unmodifiableMap(props);
    }

    @Captor
    ArgumentCaptor<KeyStoreData> keystoreDataCaptor;

    @Mock
    LogicalResponse mockResponse;

    @Mock
    KeyStoreCreator mockCreator;

    @Mock
    KeyStore mockKeyStore;

    @Mock
    VaultHelper mockVaultHelper;

    @Test
    void configureComplete() {
        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider(mockCreator, null));
        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());

        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        toTest.configure(props);

        verify(toTest, times(1)).createVaultHelper(any());
        verify(toTest, never()).get(anyString());
        verify(toTest, never()).get(anyString(), anySet());
    }

    @Test
    void configureMissing() {
        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider(mockCreator, null));

        Map<String, Object> props = new HashMap<>();

        assertThrows(ConfigException.class, () -> toTest.configure(props));
    }

    @Test
    void getAllKeysMissingKeyName() {
        String path = "/over/there";
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");
        toReturn.put(EXPECTED_CERTIFICATE_CHAIN_KEYNAME,
                PEM_KEY_CERTIFICATES);

        doReturn(toReturn).when(mockResponse).getData();
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider(mockCreator, null));
        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());
        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        toTest.configure(props);

        assertThrows(VaultKeyStoreProviderException.class, () -> toTest.get(path));
    }

    @Test
    void getAllKeysMissingCertificateChainName() {
        String path = "/over/there";
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");
        toReturn.put(EXPECTED_PRIVATE_KEY_KEYNAME,
                PEM_KEY_UNENCRYPTED);

        doReturn(toReturn).when(mockResponse).getData();
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider(mockCreator, null));
        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());
        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        toTest.configure(props);

        assertThrows(VaultKeyStoreProviderException.class, () -> toTest.get(path));
        verify(mockResponse, times(1)).getData();
        verify(mockVaultHelper, times(1)).getData(anyString());
    }

    @Test
    void getSomeKeys() throws IOException {
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");
        toReturn.put(EXPECTED_PRIVATE_KEY_KEYNAME,
                PEM_KEY_UNENCRYPTED);
        toReturn.put(EXPECTED_CERTIFICATE_CHAIN_KEYNAME,
                PEM_KEY_CERTIFICATES);

        Set<String> expectedKeys = new HashSet<>();
        expectedKeys.add("key1");

        Set<String> unexpectedKeys = new HashSet<>();
        unexpectedKeys.add(EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        unexpectedKeys.add(EXPECTED_PRIVATE_KEY_KEYNAME);
        unexpectedKeys.add("key2");
        unexpectedKeys.add("key3");

        String path = "/over/there";

        doReturn(toReturn).when(mockResponse).getData();
        doReturn(mockKeyStore).when(mockCreator)
                .createKeystore(any(KeyStoreData.class), anyBoolean());
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider(mockCreator, null));
        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());

        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        toTest.configure(props);
        ConfigData result = toTest.get(path, expectedKeys);

        verify(mockCreator, times(1)).createKeystore(keystoreDataCaptor.capture(), eq(true));

        assertNotNull(keystoreDataCaptor.getValue());
        assertNotNull(keystoreDataCaptor.getValue().getEntry(VaultKeyStoreProvider.KEY_ALIAS));
        assertTrue(keystoreDataCaptor.getValue()
                .getEntry(VaultKeyStoreProvider.KEY_ALIAS) instanceof KeyData);
        assertEquals(PEM_KEY_UNENCRYPTED,
                ((KeyData) keystoreDataCaptor.getValue().getEntry(VaultKeyStoreProvider.KEY_ALIAS))
                        .getKeyPem().get());
        assertEquals(PEM_KEY_CERTIFICATES,
                keystoreDataCaptor.getValue().getEntry(VaultKeyStoreProvider.KEY_ALIAS).getCertPem()
                        .get());

        final String location = keystoreDataCaptor.getValue().getLocation().get().toString();
        final Password password = keystoreDataCaptor.getValue().getKeystorePassword().get();

        assertNotNull(result);
        assertNotNull(result.data());

        assertEquals(location, result.data().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG));
        assertEquals(password.value(),
                result.data().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG));
        assertEquals(password.value(), result.data().get(SslConfigs.SSL_KEY_PASSWORD_CONFIG));

        assertEquals(EXPECTED_TRUSTSTORE_LOCATION,
                result.data().get(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
        assertEquals(EXPECTED_TRUSTSTORE_PASSWORD,
                result.data().get(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));

        for (String expectedKey : expectedKeys) {
            assertTrue(result.data().containsKey(expectedKey), "Expected key " + expectedKey);
            assertEquals(toReturn.get(expectedKey), result.data().get(expectedKey));
        }

        for (String unexpectedKey : unexpectedKeys) {
            assertFalse(result.data().containsKey(unexpectedKey),
                    "Found unexpected key" + unexpectedKey);
        }

    }

    @Test
    void getAllKeys()
            throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");
        toReturn.put(EXPECTED_PRIVATE_KEY_KEYNAME,
                PEM_KEY_UNENCRYPTED);
        toReturn.put(EXPECTED_CERTIFICATE_CHAIN_KEYNAME,
                PEM_KEY_CERTIFICATES);

        Set<String> expectedKeys = new HashSet<>(toReturn.keySet());
        expectedKeys.remove(EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        expectedKeys.remove(EXPECTED_PRIVATE_KEY_KEYNAME);

        Set<String> unexpectedKeys = new HashSet<>();
        unexpectedKeys.add(EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        unexpectedKeys.add(EXPECTED_PRIVATE_KEY_KEYNAME);

        String path = "/over/there";

        doReturn(toReturn).when(mockResponse).getData();
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        String keystoreLocation = null;
        VaultKeyStoreProvider toTest = spy(new VaultKeyStoreProvider());

        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());

        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        toTest.configure(props);
        ConfigData result = toTest.get(path);

        assertNotNull(result);
        assertNotNull(result.data());

        keystoreLocation = result.data().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG);
        assertNotNull(keystoreLocation);
        assertTrue(Files.exists(Paths.get(keystoreLocation)));

        String keystorePassword = result.data().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG);
        assertNotNull(keystorePassword);
        String keyPassword = result.data().get(SslConfigs.SSL_KEY_PASSWORD_CONFIG);
        assertNotNull(keyPassword);

        assertEquals(EXPECTED_TRUSTSTORE_LOCATION,
                result.data().get(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
        assertEquals(EXPECTED_TRUSTSTORE_PASSWORD,
                result.data().get(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));

        for (String expectedKey : expectedKeys) {
            assertTrue(result.data().containsKey(expectedKey), "Expected key " + expectedKey);
            assertEquals(toReturn.get(expectedKey), result.data().get(expectedKey));
        }

        for (String unexpectedKey : unexpectedKeys) {
            assertFalse(result.data().containsKey(unexpectedKey),
                    "Found unexpected key" + unexpectedKey);
        }

        try (FileInputStream inputStream = new FileInputStream(keystoreLocation)) {
            KeyStore ksFromFile = KeyStore.getInstance("JKS");
            ksFromFile.load(inputStream, keystorePassword.toCharArray());

            // Verify key and key certificate chains from file
            Certificate[] certificates = ksFromFile
                    .getCertificateChain(VaultKeyStoreProvider.KEY_ALIAS);
            Key key = ksFromFile
                    .getKey(VaultKeyStoreProvider.KEY_ALIAS, keyPassword.toCharArray());

            assertKeyUnencrypted(key);
            assertKeyCertificates(certificates);
        }
    }


    @Test
    void getAllKeysTwice() {
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");
        toReturn.put(EXPECTED_PRIVATE_KEY_KEYNAME,
                PEM_KEY_UNENCRYPTED);
        toReturn.put(EXPECTED_CERTIFICATE_CHAIN_KEYNAME,
                PEM_KEY_CERTIFICATES);

        Set<String> expectedKeys = new HashSet<>(toReturn.keySet());
        expectedKeys.remove(EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        expectedKeys.remove(EXPECTED_PRIVATE_KEY_KEYNAME);

        Set<String> unexpectedKeys = new HashSet<>();
        unexpectedKeys.add(EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
        unexpectedKeys.add(EXPECTED_PRIVATE_KEY_KEYNAME);

        String path = "/over/there";

        doReturn(toReturn).when(mockResponse).getData();
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        VaultKeyStoreProvider provider = spy(new VaultKeyStoreProvider());
        doReturn(mockVaultHelper).when(provider).createVaultHelper(any());

        Map<String, Object> props = new HashMap<>(CONFIG_MINUMUM);
        provider.configure(props);
        ConfigData result1 = provider.get(path);
        ConfigData result2 = provider.get(path);

        assertNotNull(result1);
        assertNotNull(result1.data());

        assertNotNull(result2);
        assertNotNull(result2.data());

        String keystoreLocation1 = result1.data().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG);
        String keystoreLocation2 = result2.data().get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG);
        assertNotNull(keystoreLocation1);
        assertTrue(Files.exists(Paths.get(keystoreLocation1)));
        assertEquals(keystoreLocation1, keystoreLocation2);

        String keystorePassword1 = result1.data().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG);
        String keystorePassword2 = result2.data().get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG);
        assertNotNull(keystorePassword1);
        assertEquals(keystorePassword1, keystorePassword2);

        String keyPassword1 = result1.data().get(SslConfigs.SSL_KEY_PASSWORD_CONFIG);
        String keyPassword2 = result2.data().get(SslConfigs.SSL_KEY_PASSWORD_CONFIG);
        assertNotNull(keyPassword1);
        assertEquals(keyPassword1, keyPassword2);
    }
}
