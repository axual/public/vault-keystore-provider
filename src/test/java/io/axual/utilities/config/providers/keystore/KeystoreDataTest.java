package io.axual.utilities.config.providers.keystore;

/*-
 * ========================LICENSE_START=================================
 * vault-keystore-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;

class KeystoreDataTest {

  public static final Path EXPECTED_PATH = Paths.get("/tmp/somewhere");
  public static final Password EXPECTED_KEYSTORE_PASSWORD = new Password("pass");
  public static final String EXPECTED_KEY_1 = "key1";
  public static final String EXPECTED_KEY_CERT_1 = "keycert1";
  public static final Password EXPECTED_KEYPASSWORD_1 = new Password("pass1");
  public static final String EXPECTED_KEY_2 = "key2";
  public static final String EXPECTED_KEY_CERT_2 = "keycert2";
  public static final Password EXPECTED_KEYPASSWORD_2 = new Password("pass2");
  public static final String EXPECTED_CERT_1 = "cert1";

  public static final String KEYDATA_NAME_1 = "keyData1";
  public static final KeyData KEYDATA_1 = new KeyData(EXPECTED_KEY_1, EXPECTED_KEY_CERT_1,
      EXPECTED_KEYPASSWORD_1);
  public static final String KEYDATA_NAME_2 = "keyData2";
  public static final KeyData KEYDATA_2 = new KeyData(EXPECTED_KEY_2, EXPECTED_KEY_CERT_2,
      EXPECTED_KEYPASSWORD_2);
  public static final String CERTDATA_NAME_1 = "certData1";
  public static final CertificateData CERTDATA_1 = new CertificateData(EXPECTED_CERT_1);


  private KeyStoreData getData() {
    return new KeyStoreData(EXPECTED_PATH, EXPECTED_KEYSTORE_PASSWORD);
  }

  @Test
  void getLocation() {
    assertEquals(EXPECTED_PATH, getData().getLocation().get());
  }

  @Test
  void getLocationNull() {
    KeyStoreData data = new KeyStoreData(null, null);
    assertFalse(data.getLocation().isPresent());
  }

  @Test
  void getKeystorePassword() {
    assertEquals(EXPECTED_KEYSTORE_PASSWORD, getData().getKeystorePassword().get());
  }

  @Test
  void getKeystorePasswordNull() {
    KeyStoreData data = new KeyStoreData(null, null);
    assertFalse(data.getKeystorePassword().isPresent());
  }

  @Test
  void getEntryNamesNone() {
    Collection<String> entryNames = getData().getEntryNames();
    assertNotNull(entryNames);
    assertTrue(entryNames.isEmpty());
  }

  @Test
  void getEntryNamesWithEntries() {
    final int EXPECTED_SIZE = 3;
    KeyStoreData data = getData();
    data.putEntry(KEYDATA_NAME_1, KEYDATA_1);
    data.putEntry(KEYDATA_NAME_2, KEYDATA_2);
    data.putEntry(CERTDATA_NAME_1, CERTDATA_1);
    Collection<String> entryNames = data.getEntryNames();
    assertNotNull(entryNames);
    assertTrue(entryNames.contains(KEYDATA_NAME_1));
    assertTrue(entryNames.contains(KEYDATA_NAME_2));
    assertTrue(entryNames.contains(CERTDATA_NAME_1));
    assertEquals(EXPECTED_SIZE, entryNames.size());
  }

  @Test
  void getCertificateEntry() {
    KeyStoreData data = getData();
    data.putEntry(KEYDATA_NAME_1, KEYDATA_1);
    data.putEntry(KEYDATA_NAME_2, KEYDATA_2);
    data.putEntry(CERTDATA_NAME_1, CERTDATA_1);
    CertificateData certificateEntry = data.getEntry(CERTDATA_NAME_1);
    assertEquals(CERTDATA_1, certificateEntry);
  }

  @Test
  void getKeyEntry() {
    KeyStoreData data = getData();
    data.putEntry(KEYDATA_NAME_1, KEYDATA_1);
    data.putEntry(KEYDATA_NAME_2, KEYDATA_2);
    data.putEntry(CERTDATA_NAME_1, CERTDATA_1);
    CertificateData certificateEntry = data.getEntry(KEYDATA_NAME_1);
    assertEquals(KEYDATA_1, certificateEntry);
  }

  @Test
  void putEntry() {
    KeyStoreData data = getData();
    assertNull(data.putEntry(KEYDATA_NAME_1, KEYDATA_1));
    assertTrue(data.getEntryNames().contains(KEYDATA_NAME_1));
    assertEquals(KEYDATA_1, data.getEntry(KEYDATA_NAME_1));
  }

  @Test
  void replaceEntry() {
    KeyStoreData data = getData();
    assertNull(data.putEntry(KEYDATA_NAME_1, KEYDATA_1));
    assertTrue(data.getEntryNames().contains(KEYDATA_NAME_1));
    assertEquals(KEYDATA_1, data.getEntry(KEYDATA_NAME_1));
    assertEquals(KEYDATA_1, data.putEntry(KEYDATA_NAME_1, KEYDATA_2));
    assertTrue(data.getEntryNames().contains(KEYDATA_NAME_1));
    assertEquals(KEYDATA_2, data.getEntry(KEYDATA_NAME_1));
  }
}
