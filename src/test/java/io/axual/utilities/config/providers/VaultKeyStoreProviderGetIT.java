package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyCertificates;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.assertKeyUnencrypted;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.axual.utilities.config.providers.util.VaultTestContainer;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.common.config.ConfigData;
import org.apache.kafka.common.config.SslConfigs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
class VaultKeyStoreProviderGetIT {

  public static final Logger LOG = LoggerFactory.getLogger(VaultKeyStoreProviderGetIT.class);

  @Container
  VaultTestContainer vaultContainer = VaultTestContainer.getInstance();

  @BeforeEach
  void prepare() {
    vaultContainer.start();
  }

  @Test
  void getAll()
      throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
    LOG.info("Start test");
    final String address = vaultContainer.getAddress();
    final String secretId = vaultContainer.getSecretId();
    final String roleId = vaultContainer.getRoleId();
    final String truststoreLocation = "/tmp/sometrust.jsk";
    final String truststorePassword = "truststorePassword";

    Map<String, Object> props = new HashMap<>();
    props.put(VaultKeyStoreProviderConfig.VAULT_ADDRESS_CONFIG, address);
    props.put(VaultKeyStoreProviderConfig.VAULT_AUTH_METHOD_CONFIG,
        VaultKeyStoreProviderConfig.VAULT_AUTH_METHOD_APPROLE);
    props.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, roleId);
    props.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, secretId);
    props.put(VaultKeyStoreProviderConfig.KEYSTORE_PRIVATE_KEY_KEYNAME_CONFIG,
        VaultTestContainer.RESOURCE_DATA_PRIVATE_KEY_NAME);
    props.put(VaultKeyStoreProviderConfig.KEYSTORE_CERTIFICATE_CHAIN_KEYNAME_CONFIG,
        VaultTestContainer.RESOURCE_DATA_CERTIFICATE_CHAIN_NAME);
    props.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_LOCATION_CONFIG,
        truststoreLocation);
    props.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_PASSWORD_CONFIG,
        truststorePassword);

    LOG.info("Prepared configuration");

    try (VaultKeyStoreProvider provider = new VaultKeyStoreProvider()) {
      provider.configure(props);
      LOG.info("Configured provider");

      ConfigData data = provider.get(VaultTestContainer.RESOURCE_DATA_PATH);
      LOG.info("Retrieved Vault Data {}", data.data());

      assertNotNull(data);
      Map<String, String> configData = data.data();
      assertNotNull(configData);
      assertFalse(configData.containsKey(
          VaultTestContainer.RESOURCE_DATA_CERTIFICATE_CHAIN_NAME));
      assertFalse(configData
          .containsKey(VaultTestContainer.RESOURCE_DATA_PRIVATE_KEY_NAME));

      assertTrue(
          configData
              .containsKey(VaultTestContainer.RESOURCE_DATA_KEY_ONE));
      assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_ONE,
          configData.get(VaultTestContainer.RESOURCE_DATA_KEY_ONE));

      assertTrue(
          configData
              .containsKey(VaultTestContainer.RESOURCE_DATA_KEY_TWO));
      assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_TWO,
          configData.get(VaultTestContainer.RESOURCE_DATA_KEY_TWO));

      assertTrue(
          configData
              .containsKey(VaultTestContainer.RESOURCE_DATA_KEY_THREE));
      assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_THREE,
          configData.get(VaultTestContainer.RESOURCE_DATA_KEY_THREE));

      assertFalse(configData.containsKey(
          VaultTestContainer.RESOURCE_DATA_CERTIFICATE_CHAIN_NAME));
      assertFalse(configData
          .containsKey(
              VaultTestContainer.RESOURCE_DATA_PRIVATE_KEY_NAME));

      assertTrue(
          configData
              .containsKey(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
      String returnedTruststoreLocation = configData
          .get(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG);
      assertEquals(truststoreLocation, returnedTruststoreLocation);

      assertTrue(
          configData
              .containsKey(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));
      String returnedTruststorePassword = configData
          .get(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG);
      assertEquals(truststorePassword, returnedTruststorePassword);

      assertTrue(
          configData
              .containsKey(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG));
      String returnedKeystoreLocation = configData
          .get(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG);

      assertTrue(
          configData
              .containsKey(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG));
      String returnedKeystorePassword = configData
          .get(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG);

      assertTrue(
          configData
              .containsKey(SslConfigs.SSL_KEY_PASSWORD_CONFIG));
      String returnedKeyPassword = configData
          .get(SslConfigs.SSL_KEY_PASSWORD_CONFIG);

      try (FileInputStream inputStream = new FileInputStream(returnedKeystoreLocation)) {
        KeyStore ksFromFile = KeyStore.getInstance("JKS");
        ksFromFile.load(inputStream, returnedKeystorePassword.toCharArray());

        // Verify key and key certificate chains from file
        Certificate[] certificates = ksFromFile
            .getCertificateChain(VaultKeyStoreProvider.KEY_ALIAS);
        Key key = ksFromFile
            .getKey(VaultKeyStoreProvider.KEY_ALIAS, returnedKeyPassword.toCharArray());

        assertKeyUnencrypted(key);
        assertKeyCertificates(certificates);
      }
    }
  }

}
