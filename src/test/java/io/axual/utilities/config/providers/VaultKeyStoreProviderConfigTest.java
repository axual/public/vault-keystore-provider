package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VaultKeyStoreProviderConfigTest {

  public static final Map<String, Object> CONFIG_BASE;
  public static final String EXPECTED_ADDRESS = "http://localvault:1234";
  public static final String EXPECTED_SECRET_ID = "some-secret-id";
  public static final String EXPECTED_APPROLE_ID = "some-approle-id";
  public static final String EXPECTED_APPROLE_PATH = "/testing/login/approle";

  public static final Map<String, Object> CONFIG_ALL;
  public static final String EXPECTED_PRIVATE_KEY_KEYNAME = "testPrivateKey";
  public static final String EXPECTED_CERTIFICATE_CHAIN_KEYNAME = "testCertificateChain";
  public static final String EXPECTED_TEMPORARY_STORAGE_DIRECTORY = "/temp/storage";
  public static final String EXPECTED_TRUSTSTORE_LOCATION = "/temp/truststore";
  public static final String EXPECTED_TRUSTSTORE_PASSWORD = "trust this";
  public static final Map<String, Object> CONFIG_EXTENDED;
  private static final String EXTENDED_NAME_CONFIG = "keyName";
  private static final String EXPECTED_EXTENDED_NAME = "testName";
  private static final ConfigDef EXTENSION_DEF = new ConfigDef()
      .define(EXTENDED_NAME_CONFIG, ConfigDef.Type.STRING, ConfigDef.Importance.HIGH,
          "Test Config");
  private static final ConfigDef EXTENDED_DEF = VaultKeyStoreProviderConfig
      .addVaultKeystoreProviderConfigDefinitions(new ConfigDef(EXTENSION_DEF));

  static {
    final Map<String, Object> base = new HashMap<>();
    base.put(VaultKeyStoreProviderConfig.VAULT_ADDRESS_CONFIG, EXPECTED_ADDRESS);
    base.put(VaultKeyStoreProviderConfig.VAULT_AUTH_METHOD_CONFIG,
        VaultKeyStoreProviderConfig.VAULT_AUTH_METHOD_APPROLE);
    base.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_PATH_CONFIG,
        EXPECTED_APPROLE_PATH);
    base.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG,
        EXPECTED_APPROLE_ID);
    base.put(VaultKeyStoreProviderConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG,
        EXPECTED_SECRET_ID);
    CONFIG_BASE = Collections.unmodifiableMap(base);

    final Map<String, Object> all = new HashMap<>(CONFIG_BASE);
    all.put(VaultKeyStoreProviderConfig.KEYSTORE_PRIVATE_KEY_KEYNAME_CONFIG,
        EXPECTED_PRIVATE_KEY_KEYNAME);
    all.put(VaultKeyStoreProviderConfig.KEYSTORE_CERTIFICATE_CHAIN_KEYNAME_CONFIG,
        EXPECTED_CERTIFICATE_CHAIN_KEYNAME);
    all.put(VaultKeyStoreProviderConfig.KEYSTORE_TEMPORARY_STORAGE_DIRECTORY_CONFIG,
        EXPECTED_TEMPORARY_STORAGE_DIRECTORY);
    all.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_LOCATION_CONFIG,
        EXPECTED_TRUSTSTORE_LOCATION);
    all.put(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_PASSWORD_CONFIG,
        EXPECTED_TRUSTSTORE_PASSWORD);
    CONFIG_ALL = Collections.unmodifiableMap(all);

    final Map<String, Object> extended = new HashMap<>(CONFIG_ALL);
    extended.put(EXTENDED_NAME_CONFIG, EXPECTED_EXTENDED_NAME);
    CONFIG_EXTENDED = Collections.unmodifiableMap(extended);
  }

  @Test
  void checkConstructorsExtendedSuccess() {
    Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
    assertDoesNotThrow(() -> new VaultKeyStoreProviderConfig(EXTENDED_DEF, props));
    assertDoesNotThrow(() -> new VaultKeyStoreProviderConfig(EXTENDED_DEF, props, true));
    assertDoesNotThrow(() -> new VaultKeyStoreProviderConfig(props));
    assertDoesNotThrow(() -> new VaultKeyStoreProviderConfig(props, true));
  }

  @Test
  void checkConstructorsExtendedFailOnConfig() {
    Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
    props.remove(EXTENDED_NAME_CONFIG);
    assertThrows(ConfigException.class,
        () -> new VaultKeyStoreProviderConfig(EXTENDED_DEF, props));
    assertThrows(ConfigException.class,
        () -> new VaultKeyStoreProviderConfig(EXTENDED_DEF, props, true));
  }

  @Test
  void checkConstructorsExtendedFailOnDefinition() {
    Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
    ConfigDef emptyDef = new ConfigDef();
    assertThrows(IllegalArgumentException.class,
        () -> new VaultKeyStoreProviderConfig(emptyDef, props));
    assertThrows(IllegalArgumentException.class,
        () -> new VaultKeyStoreProviderConfig(emptyDef, props, true));
  }

  @Test
  void checkEquality() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config1 = new VaultKeyStoreProviderConfig(props);
    VaultKeyStoreProviderConfig config2 = new VaultKeyStoreProviderConfig(props);

    assertNotNull(config1);
    assertNotNull(config2);
    assertEquals(config1, config2);
    assertEquals(config1.hashCode(), config2.hashCode());
  }

  @Test
  void checkInequality() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config1 = new VaultKeyStoreProviderConfig(props);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_TEMPORARY_STORAGE_DIRECTORY_CONFIG);
    VaultKeyStoreProviderConfig config2 = new VaultKeyStoreProviderConfig(props);

    assertNotNull(config1);
    assertNotNull(config1);
    assertNotNull(config2);
    assertNotEquals(config1, config2);
    assertNotEquals(config1.hashCode(), config2.hashCode());
  }

  @Test
  void getPrivateKeyKeyNameSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(EXPECTED_PRIVATE_KEY_KEYNAME, config::getPrivateKeyKeyName);
  }

  @Test
  void getPrivateKeyKeyNameNotSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_PRIVATE_KEY_KEYNAME_CONFIG);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(VaultKeyStoreProviderConfig.KEYSTORE_PRIVATE_KEY_KEYNAME_DEFAULT,
        config::getPrivateKeyKeyName);
  }

  @Test
  void getCertificateKeyNameSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(EXPECTED_CERTIFICATE_CHAIN_KEYNAME, config::getCertificateChainKeyName);
  }

  @Test
  void getCertificateKeyNameNotSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_CERTIFICATE_CHAIN_KEYNAME_CONFIG);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(VaultKeyStoreProviderConfig.KEYSTORE_CERTIFICATE_CHAIN_KEYNAME_DEFAULT,
        config::getCertificateChainKeyName);
  }

  @Test
  void getTemporaryStorageDirectorySet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(EXPECTED_TEMPORARY_STORAGE_DIRECTORY,
        config::getTemporaryStorageDirectory);
  }

  @Test
  void getTemporaryStorageDirectoryNotSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_TEMPORARY_STORAGE_DIRECTORY_CONFIG);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(VaultKeyStoreProviderConfig.KEYSTORE_TEMPORARY_STORAGE_DIRECTORY_DEFAULT,
        config::getTemporaryStorageDirectory);
  }

  @Test
  void getTrustStoreLocationSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(EXPECTED_TRUSTSTORE_LOCATION, config::getTrustStoreLocation);
  }

  @Test
  void getTrustStoreLocationNotSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_LOCATION_CONFIG);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalNotSet(config::getTrustStoreLocation);
  }

  @Test
  void getTrustStorePasswordSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalSet(new Password(EXPECTED_TRUSTSTORE_PASSWORD),
        config::getTrustStorePassword);
  }

  @Test
  void getTrustStorePasswordNotSet() {
    Map<String, Object> props = new HashMap<>(CONFIG_ALL);
    props.remove(VaultKeyStoreProviderConfig.KEYSTORE_TRUSTSTORE_PASSWORD_CONFIG);
    VaultKeyStoreProviderConfig config = new VaultKeyStoreProviderConfig(props);

    assertOptionalNotSet(config::getTrustStorePassword);
  }

  private <T> void assertOptionalSet(T expected, Supplier<Optional<T>> supplier) {
    Optional<T> value = supplier.get();
    assertNotNull(value);
    assertTrue(value.isPresent());
    assertEquals(expected, value.get());
  }

  private <T> void assertOptionalNotSet(Supplier<Optional<T>> supplier) {
    Optional<T> value = supplier.get();
    assertNotNull(value);
    assertFalse(value.isPresent());
  }
}
