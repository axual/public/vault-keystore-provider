package io.axual.utilities.config.providers.util;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_CERTIFICATES;
import static io.axual.utilities.config.providers.util.KeyAndCertHelper.PEM_KEY_UNENCRYPTED;

import com.github.dockerjava.api.command.InspectContainerResponse;
import java.io.IOException;
import java.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.shaded.org.apache.commons.lang3.ArrayUtils;
import org.testcontainers.utility.MountableFile;
import org.testcontainers.vault.VaultContainer;

public class VaultTestContainer extends VaultContainer<VaultTestContainer> {

  private static final Logger LOG = LoggerFactory.getLogger(VaultTestContainer.class);
  private static final String IMAGE_VERSION = "vault:1.4.2";

  private static final String POLICY_FILE_PATH = "/vault/config/policy.hcl";
  private static final String POLICY_FILE_RESOURCE = "policy.hcl";
  private static final String ROOT_TOKEN = "my-root-token";
  private static final String RESOURCE_PATH = "resources";
  private static final String AUTH_METHOD = "approle";
  private static final String ROLE_NAME = "connect-role";
  private static final String POLICY_NAME = "connect-role-policy";
  private static final String ROLE_PATH = "auth/approle/role/" + ROLE_NAME + "/role-id";
  private static final String SECRET_PATH = "auth/approle/role/" + ROLE_NAME + "/secret-id";
  private static final String LOGIN_PATH = "auth/approle/login";
  public static final Integer PORT_NUMBER = 8200;

  public static final String RESOURCE_DATA_PATH = RESOURCE_PATH + "/test/one";
  public static final String RESOURCE_DATA_KEY_ONE = "one";
  public static final String RESOURCE_DATA_VALUE_ONE = "value one";
  public static final String RESOURCE_DATA_KEY_TWO = "two";
  public static final String RESOURCE_DATA_VALUE_TWO = "value two";
  public static final String RESOURCE_DATA_KEY_THREE = "three";
  public static final String RESOURCE_DATA_VALUE_THREE = "value three";
  public static final String RESOURCE_DATA_PRIVATE_KEY_NAME = "private.key";
  public static final String RESOURCE_DATA_CERTIFICATE_CHAIN_NAME = "certificate.chain";

  private static final String[] SECRETS;

  static {

    SECRETS = new String[]{
        String.join("=", RESOURCE_DATA_KEY_ONE, RESOURCE_DATA_VALUE_ONE),
        String.join("=", RESOURCE_DATA_KEY_TWO, RESOURCE_DATA_VALUE_TWO),
        String.join("=", RESOURCE_DATA_KEY_THREE, RESOURCE_DATA_VALUE_THREE),
        String.join("=", RESOURCE_DATA_CERTIFICATE_CHAIN_NAME, PEM_KEY_CERTIFICATES),
        String.join("=", RESOURCE_DATA_PRIVATE_KEY_NAME, PEM_KEY_UNENCRYPTED)
    };
  }

  private VaultTestContainer() {
    super(IMAGE_VERSION);
  }

  private static VaultTestContainer vaultContainer;

  public synchronized static VaultTestContainer getInstance() {
    if (vaultContainer == null) {
      vaultContainer = new VaultTestContainer();
    }
    return vaultContainer;
  }

  @Override
  protected void containerIsStopped(InspectContainerResponse containerInfo) {
    LOG.info("Container stopped {}", containerInfo);
    super.containerIsStopped(containerInfo);
    dockerClient.removeContainerCmd(containerInfo.getId());
  }

  @Override
  public void start() {
    if (!vaultContainer.isRunning()) {
      vaultContainer
          .withReuse(false)
          .withStartupAttempts(4)
          .withStartupTimeout(Duration.ofMinutes(1))
          .withExposedPorts(PORT_NUMBER)
          .withCopyFileToContainer(MountableFile.forClasspathResource(POLICY_FILE_RESOURCE),
              POLICY_FILE_PATH)
          .withVaultToken(ROOT_TOKEN);
      super.start();
      initializeVaultContainer();
    }
  }


  /**
   * Initialize vault for the following : 1. Enable app role auth 2. Create mgmt-api Policy , Role
   * and Secret 3. Initialize connectors secret engine with KV2 4. Create Private key in vault for
   * Test app Rabo-app 6
   */
  private void initializeVaultContainer() {
    try {
      ExecResult authEnableResult = vaultContainer
          .execInContainer("vault", "auth", "enable", AUTH_METHOD);
      LOG.info("authEnableResult = {}", authEnableResult.getStdout().replace("\n", ""));

      ExecResult policyWritePolicyResult = vaultContainer
          .execInContainer("vault", "policy", "write", POLICY_NAME, POLICY_FILE_PATH);
      LOG.info("policyWritePolicyResult = {}",
          policyWritePolicyResult.getStdout().replace("\n", ""));

      ExecResult policyWriteToleResult = vaultContainer
          .execInContainer("vault", "write", "auth/approle/role/" + ROLE_NAME,
              "token_policies=" + POLICY_NAME);
      LOG.info("policyWriteToleResult = {}", policyWriteToleResult.getStdout().replace("\n", ""));

      ExecResult vaultReadRoleId = vaultContainer.execInContainer("vault", "read", ROLE_PATH);
      LOG.info("vaultReadRoleId = {}", vaultReadRoleId.getStdout().replace("\n", ""));
      String roleId = vaultReadRoleId.getStdout().replace("\n", "").split("role_id")[1]
          .split("\n")[0].trim();

      ExecResult vaultWriteSecretId = vaultContainer
          .execInContainer("vault", "write", "-f", SECRET_PATH);
      LOG.info("vaultWriteSecretId = {}", vaultWriteSecretId.getStdout().replace("\n", ""));
      String secret = vaultWriteSecretId.getStdout().replace("\n", "").split("secret_id")[1].trim();

      ExecResult vaultWriteLoginPath = vaultContainer.execInContainer("vault", "write",
          LOGIN_PATH,
          "role_id=" + roleId,
          "secret_id=" + secret);
      LOG.info("vaultWriteLoginPath = {}", vaultWriteSecretId.getStdout().replace("\n", ""));

      //Create KV2 secret engine called connectors
      vaultContainer
          .execInContainer("vault", "secrets", "enable", "-path=" + RESOURCE_PATH, "kv-v2");
      // Add private key to vault for rabo-app-6 created via data.sql

      String[] cmd = ArrayUtils.addAll(new String[]{"vault", "kv", "put", RESOURCE_DATA_PATH}, SECRETS);
      vaultContainer.execInContainer(cmd);

      this.address = "http://" + vaultContainer.getHost() + ":" + vaultContainer.getMappedPort(PORT_NUMBER);
      this.roleId = roleId;
      this.secretId = secret;
      LOG.info("address {}", address);
      LOG.info("roleId {}", roleId);
      LOG.info("secret {}", secret);
    } catch (IOException | InterruptedException e) {
      LOG.error("Failed to initialize the vault test container", e);
    }
  }

  @Override
  public void stop() {
    //do nothing, JVM handles shut down
  }

  private String address;

  public String getAddress() {
    return address;
  }

  private String secretId;

  public String getSecretId() {
    return secretId;
  }

  private String roleId;

  public String getRoleId() {
    return roleId;
  }


}
