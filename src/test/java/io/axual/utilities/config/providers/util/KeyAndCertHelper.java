package io.axual.utilities.config.providers.util;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.IOException;
import java.security.Key;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Assertions;

public class KeyAndCertHelper {

  public static final Integer PEM_CA_COUNT = 1;
  public static final String PEM_CA;
  public static final String PEM_CA_FINGERPRINT = cleanFingerprint(
      "95:99:2A:A7:1F:43:B2:A6:4C:C6:4F:90:7B:B9:CB:84:9C:6D:40:D9");
  public static final Integer PEM_MULTIPLE_CA_COUNT = 2;
  public static final String PEM_MULTIPLE_CA;
  public static final String PEM_MULTIPLE_CA_FINGERPRINT_0 = cleanFingerprint(
      "95:99:2A:A7:1F:43:B2:A6:4C:C6:4F:90:7B:B9:CB:84:9C:6D:40:D9");
  public static final String PEM_MULTIPLE_CA_FINGERPRINT_1 = cleanFingerprint(
      "95:99:2A:A7:1F:43:B2:A6:4C:C6:4F:90:7B:B9:CB:84:9C:6D:40:D9");

  public static final Integer PEM_KEY_CERTIFICATES_COUNT = 2;
  public static final String PEM_KEY_CERTIFICATES;
  public static final String PEM_KEY_CERTIFICATES_FINGERPRINT_0 = cleanFingerprint(
      "8E:2C:64:91:0B:F6:29:80:2A:DA:83:0C:7A:03:F3:2A:E1:96:11:02");
  public static final String PEM_KEY_CERTIFICATES_FINGERPRINT_1 = cleanFingerprint(
      "95:99:2A:A7:1F:43:B2:A6:4C:C6:4F:90:7B:B9:CB:84:9C:6D:40:D9");
  public static final String PEM_KEY_UNENCRYPTED;
  public static final String PEM_KEY_UNENCRYPTED_FINGERPRINT_1 = cleanFingerprint(
      "14:F9:D2:22:55:9F:70:36:52:2A:A8:DB:DC:0B:59:D5:EC:64:B6:ED");

  public static final String PEM_KEY_ENCRYPTED;

  static {
    try {
      PEM_CA = Resources.toString(Resources.getResource("ssl/ca.cer"), Charsets.UTF_8);
      PEM_MULTIPLE_CA = Resources
          .toString(Resources.getResource("ssl/ca_multiple.cer"), Charsets.UTF_8);
      PEM_KEY_CERTIFICATES = Resources
          .toString(Resources.getResource("ssl/key.cer"), Charsets.UTF_8);
      PEM_KEY_ENCRYPTED = Resources
          .toString(Resources.getResource("ssl/key_encrypted.pem"), Charsets.UTF_8);
      PEM_KEY_UNENCRYPTED = Resources
          .toString(Resources.getResource("ssl/key_unencrypted.pem"), Charsets.UTF_8);
    } catch (IOException ioe) {
      throw new RuntimeException("Could not load certificates", ioe);
    }
  }


  /**
   * Cleans the fingerprint by making it lowercase and removing the colon separator
   *
   * @param fingerprint the fingerprint scan to clean.
   * @return the cleaned fingerprint
   */
  public static String cleanFingerprint(final String fingerprint) {
    return fingerprint.toLowerCase().replaceAll(":", "");
  }

  /**
   * Creates a SHA1 Fingerprint
   *
   * @param encoded the data to fingerprint
   * @return SHA-1 HexBinary fingerprint in lowercase and with colon separator
   */
  public static String fingerprint(byte[] encoded) {
    return cleanFingerprint(DigestUtils.sha1Hex(encoded));
  }

  public static void assertMultipleCACertificate(Certificate... certificates)
      throws CertificateEncodingException {
    assertNotNull(certificates);
    Assertions.assertEquals(PEM_MULTIPLE_CA_COUNT, certificates.length);
    assertCertificate(certificates[0], PEM_MULTIPLE_CA_FINGERPRINT_0);
    assertCertificate(certificates[1], PEM_MULTIPLE_CA_FINGERPRINT_1);
  }

  public static void assertCACertificate(Certificate[] certificates)
      throws CertificateEncodingException {
    assertNotNull(certificates);
    Assertions.assertEquals(PEM_CA_COUNT, certificates.length);
    assertCACertificate(certificates[0]);
  }

  public static void assertCACertificate(Certificate certificate)
      throws CertificateEncodingException {
    assertCertificate(certificate, PEM_CA_FINGERPRINT);
  }

  public static void assertKeyCertificates(Certificate[] certificates)
      throws CertificateEncodingException {
    assertNotNull(certificates);
    assertEquals(PEM_KEY_CERTIFICATES_COUNT.intValue(), certificates.length);
    assertCertificate(certificates[0], PEM_KEY_CERTIFICATES_FINGERPRINT_0);
    assertCertificate(certificates[1], PEM_KEY_CERTIFICATES_FINGERPRINT_1);
  }

  public static void assertKeyUnencrypted(Key key) {
    assertNotNull(key);
    assertEquals(PEM_KEY_UNENCRYPTED_FINGERPRINT_1, fingerprint(key.getEncoded()));
  }

  public static void assertCertificate(Certificate cert, String fingerprint)
      throws CertificateEncodingException {
    assertNotNull(cert);
    Assertions.assertEquals(fingerprint, fingerprint(cert.getEncoded()));

  }

}
