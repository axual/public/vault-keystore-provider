package io.axual.utilities.config.providers.keystore;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.kafka.common.config.types.Password;

public class KeyStoreData {

  public final Path location;
  public final Password keystorePassword;

  public final Map<String, CertificateData> entries = new HashMap<>();

  public KeyStoreData(Path location, Password keystorePassword) {
    this.location = location;
    this.keystorePassword = keystorePassword;
  }

  public Optional<Path> getLocation() {
    return Optional.ofNullable(location);
  }

  public Optional<Password> getKeystorePassword() {
    return Optional.ofNullable(keystorePassword);
  }

  public Collection<String> getEntryNames() {
    return entries.keySet();
  }

  public CertificateData getEntry(String name) {
    return entries.get(name);
  }

  public CertificateData putEntry(String name, CertificateData data) {
    return entries.put(name, data);
  }
}
