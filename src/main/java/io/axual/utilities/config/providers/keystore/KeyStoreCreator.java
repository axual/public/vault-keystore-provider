package io.axual.utilities.config.providers.keystore;

/*-
 * ========================LICENSE_START=================================
 * Keystore Generation Configuration Provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import io.axual.utilities.config.providers.exceptions.KeyStoreCreatorException;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.kafka.common.config.types.Password;

public class KeyStoreCreator {

  public static final String CERTIFICATE_CHAIN_NAME_PATTERN = "%s_%d";
  private static final Password DEFAULT_PASSWORD = new Password("");
  public static final KeyStoreCreator INSTANCE = new KeyStoreCreator();

  public KeyStore createKeystore(final KeyStoreData keystoreData) {
    return createKeystore(keystoreData, false);
  }

  public KeyStore createKeystore(final KeyStoreData keystoreData, boolean saveToFile) {
    try {
      KeyStore keyStore = KeyStore.getInstance("jks");
      keyStore.load(null,
          keystoreData.getKeystorePassword().orElse(DEFAULT_PASSWORD).value().toCharArray());

      for (String entryName : keystoreData.getEntryNames()) {
        CertificateData data = keystoreData.getEntry(entryName);
        Certificate[] certificates = createCertificates(data);
        if (data instanceof KeyData) {
          KeyData keyData = (KeyData) data;
          // create key
          Key key = createKey(keyData);
          keyStore
              .setKeyEntry(entryName, key,
                  keyData.getKeyPassword().orElse(DEFAULT_PASSWORD).value().toCharArray(),
                  certificates);
        } else {
          if (certificates.length > 1) {
            for (int cnt = 0; cnt < certificates.length; cnt++) {
              keyStore
                  .setCertificateEntry(
                      String.format(CERTIFICATE_CHAIN_NAME_PATTERN, entryName, cnt),
                      certificates[cnt]);
            }
          } else if (certificates.length == 1) {
            keyStore
                .setCertificateEntry(
                    entryName,
                    certificates[0]);

          }
        }
      }
      if (saveToFile) {
        saveKeyStore(keyStore,
            keystoreData.getLocation()
                .orElseThrow(() -> new KeyStoreCreatorException("No location set")),
            keystoreData.getKeystorePassword().orElse(DEFAULT_PASSWORD));
      }

      return keyStore;
    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
      throw new KeyStoreCreatorException("Could not get create KeyStore", e);
    }
  }

  public void saveKeyStore(final KeyStore keyStore, Path location, Password password) {
    try (FileOutputStream fos = new FileOutputStream(location.toFile())) {
      keyStore.store(fos, password.value().toCharArray());
    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
      throw new KeyStoreCreatorException("Could not get save KeyStore", e);
    }
  }

  public Key createKey(KeyData keyData) {
    try {
      KeyFactory factory = KeyFactory.getInstance("RSA");
      String keyBase64 = keyData.getKeyPem()
          .orElseThrow(() -> new KeyStoreCreatorException("No key data set"))
          .replace("\n", "")
          .replaceAll("(-----BEGIN PRIVATE KEY-----|-----END PRIVATE KEY-----)", "");
      byte[] keyBytes = Base64.getDecoder().decode(keyBase64);
      PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
      return factory.generatePrivate(spec);
    } catch (NoSuchAlgorithmException e) {
      throw new KeyStoreCreatorException("Could not get key factory for key creation", e);
    } catch (InvalidKeySpecException e) {
      throw new KeyStoreCreatorException("Could not create private key", e);
    } catch (IllegalArgumentException e) {
      throw new KeyStoreCreatorException("Provided data is invalid", e);
    }
  }


  public Certificate[] createCertificates(CertificateData certData) {
    try {
      CertificateFactory factory = CertificateFactory.getInstance("X.509");
      String[] splitted = certData.getCertPem()
          .orElseThrow(() -> new KeyStoreCreatorException("No certificate data set"))
          .replace("\n", "")
          .split("(-----BEGIN CERTIFICATE-----|-----END CERTIFICATE-----)");
      List<String> pems = Arrays.stream(splitted).filter(s -> !(s == null || s.isEmpty()))
          .collect(Collectors.toList());
      Certificate[] certs = new Certificate[pems.size()];
      for (int i = 0; i < certs.length; i++) {
        certs[i] = factory.generateCertificate(
            new ByteArrayInputStream(Base64.getDecoder().decode(pems.get(i))));
      }
      return certs;
    } catch (CertificateException e) {
      throw new KeyStoreCreatorException(
          "Could not create certificate array from certificate data", e);
    }
  }
}
